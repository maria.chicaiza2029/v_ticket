<?php
/**
 * Estudiante, ULTIMO PROCEDMIENTO
 */
class Equipo extends CI_Model
{
  function __construct()
  {
    // code...
  		parent::__construct();
  }
  public function insertar($datos){
    //ponemos el nombre de la tabla de la BDD CREADO
    return $this->db->insert("equipo",$datos);
  }
  public function obtenerTodos(){
    $equipo=$this->db->get("equipo");
    if ($equipo->num_rows()>0) {
      return $equipo;
    } else{
      return false;
    }

  }
  //funcion para eliminar un estudianteTemporal
  public function eliminarPorId($id){
    $this->db->where("id_equ",$id);
    return $this->db->delete("equipo");
  }

  public function obtenerPorId($id){
        $this->db->where("id_equ",$id);
        $equipo=$this->db->get("equipo");
        if ($equipo->num_rows()>0) {
          return $equipo->row();
        } else {
          return false;
        }
      }

      //proceso de actualizaciom de estudientes
      public function editar($id,$datos){
        $this->db->where("id_equ",$id);
        return $this->db->update("equipo",$datos);
      }

}//cierre de la clase
