<center><h3>Stadiums Registered</h3><br></center>
<?php if ($listStadiums): ?>
  <table id="tbl-matches" class="table table-secondary">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NAME</th>
        <th class="text-center">CELLPHONE</th>
        <th class="text-center">HABILITY</th>
        <th class="text-center">ACTIONS</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listStadiums->result() as $matcheTemporary): ?>
        <tr>
          <td class="text-center"><?php echo $matcheTemporary->id_sta?></td>
          <td class="text-center"><?php echo $matcheTemporary->name_sta ?></td>
          <td class="text-center"><?php echo $matcheTemporary->address_sta ?></td>
          <td class="text-center"><?php echo $matcheTemporary->abiliti_sta ?></td>
          <td class="text-center">
          <a href="<?php echo site_url(); ?>/stadiums/edit/<?php echo $matcheTemporary->id_sta ?>"><img src="<?php echo base_url(); ?>/assets/assets/img/icon/pencil.png" title="Edit" width="25px"></a>
          <a href="<?php echo site_url(); ?>/stadiums/delete/<?php echo $matcheTemporary->id_sta ?>" ><img src="<?php echo base_url(); ?>/assets/assets/img/icon/trash.png" title="Delete" width="25px">

          </a>  </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    Not found Matches Registered
  </div>
<?php endif; ?>
<script type="text/javascript">
  $("#tbl-matches").DataTable();
</script>

<style media="screen">
  .dataTables_filter label input{
    border:3px solid red !important;
  }
</style>
