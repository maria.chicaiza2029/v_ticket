<section class="breadcrumb-section set-bg" data-setbg="<?php echo base_url(); ?>assets/img/breadcrumb-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bs-text">
                        <h2>STADIUMS</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="">
           <!-- left Contents -->
           <!-- <div class="starups"> -->
             <div class="row">
               <div class="col-md-12 text-center">

                 <legend><h3><b>List of Stadiums</b></h3></legend>
               </div>
             </div>

            <div class="row">

             <div class="col-md-12 text-right">
               <center>
               <div class="slider-btns">
                    <!-- Hero-btn -->
                    <a data-animation="fadeInLeft" data-delay="1s"class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Stadium</a>
               </div>
               <br>
               <!-- <div class="slider-btns">
                    <!-- Hero-btn -->
                    <!-- <a data-animation="fadeInLeft" data-delay="1s"  class="btn site-btn" onclick="chargelocations();">Update Location</a> -->
               </div> -->
               <!-- <button type="button" class="site-btn" data-toggle="modal" data-target="#myModal">AGREGAR SUCURSAL</button>
               <button type="button" name="button" class="site-btn" onclick="cargarSucursales();"> -->

               <!-- </button> -->
                  </center>
             </div>

            </div>
          <div class="row">


           <div class="col-md-12 text-right">
             <br>
             <div class="" id="container_list_stadiums">

             </div>
           </div>
          </div>
        </div>
           <!--Right Contents  -->
       </section>
       <!--All startups End -->
       <!-- work company Start-->
       <!-- Modal -->
       <div id="myModal" class="modal fade" role="dialog" color text="black">
         <div class="modal-dialog modal-lg">

           <!-- Modal content-->
           <div class="modal-content">
             <div class="modal-header">
               <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
               <h4 class="modal-title"><b>New Stadiums</b></h4>
             </div>
             <div class="modal-body">
               <center><h4>Form of the new Stadium</h4></center>
       				<!--<center><h3><FONT COLOR="white">NUEVO CLIENTE</FONT></h3><br></center>-->
              <!-- <center><h3><FONT COLOR="black">NUEVO CLIENTE</FONT></h3><br></center> -->
       				<form class="" enctype="multipart/form-data" action="<?php echo site_url(); ?>/stadiums/saveStadium" method="post" id="frm_new_stadium">
       		      <center><table class="" style="width:700px;">
       		        <tr>
       		          <td><label for=""><h6>Name:</h4></label></td>
       		          <td><input type="text" name="name_sta" id="name_equ" class="form-control"
       		          value="" placeholder="Input the name" required></td>
       		        </tr>
       		        <br>
                  <tr>
       		          <td><label for=""><h6>Email Address:</h4></label></td>
       		          <td><input type="email" name="address_sta" id="address_sta" class="form-control"
       		          value="" placeholder="Input the address" required autocomplete="off" color= #212529 !important></td>
       		        </tr>
       		        <br>
                  <br>
                  <tr>
       		          <td><label for=""><h6>Hability:</h4></label></td>
       		          <td><input type="text" name="abiliti_sta" id="abiliti_sta" class="form-control"
       		          value="" placeholder="Input the biliti" required autocomplete="off" color= #212529 !important></td>
       		        </tr>
                   <br>

                     <br>
       				</table></center>
             </div>
             <div class="modal-footer">
       				<div class="row">
       					<div class="col-md-12">
       						<button type="submit" name="button" class="btn btn-success">Save Stadium</button>
       						<button type="button" onclick="cerrarModal();"
       										name="button" class="btn btn-danger">Cancel</button>
       					</div>
       					</form>
       					<!--<div class="col-md-4">
       	        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       					</div>-->
             </div>
           </div>
       	</div>
         </div>
       </div>
       <script type="text/javascript">
       $("#myModal").appendTo("body");
       //Funcion para ocultar/cerrar la ventana Modal
       	function cerrarModal(){
       		$("#myModal").modal("hide");
       	}
       </script>
       <style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_new_abiliti").validate({
    rules:{
      name_sta:{
        required:true
      },
      address_sta:{
        required:true
      },
      abiliti_sta:{
        required:true
      }
    },
    messages:{
      name_sta:{
        required:"<br>Input the name please"
      },
      address_sta:{
        required:"<br>Input the address please"
      },
       abiliti_sta:{
        required:"<br>Input the abiliti please"
      }
    },
    submitHandler:function(form){
          var url=$(form).prop("action");//capturando url (controlador/funcion)
          //generando peticion asincrona
          $.ajax({
               url:url,//action del formulario
               type:'post',//definiendo el tipo de envio de datos post/get
               data:$(form).serialize(), //enviando los datos ingresados en el formulario
               success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                  //alert('Cliente guardado exitosamente');
									chargeUsers(); //llamado a la funcion para actualizar listado de clientes
									$(form)[0].reset();
									cerrarModal();
									Swal.fire({
										background: '#0C062E',
										color:'#FFF',
										title: 'Confirmación',
									  text: "User save sucessfully",
										icon:'success',
									});
               },
               error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                  //alert('Error al insertar, intente nuevamente');
               }
          });
        },
		errorElement : 'span'
  });
</script>
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Are you Sure?',
  text: "¡It will be forever!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Delete!',
  cancelButtonText:'Cancel',
	background: '#0C062E',
	color:'#FFF' }).then((result) => {
  if (result.isConfirmed) {
		chargeUsers();
    window.stadium.href = urlToRedirect;
  }
});
}
</script>
<script type="text/javascript">
          	function chargeUsers(){
          		$("#container_list_stadiums").load('<?php echo site_url("stadiums/tableStadiums") ?>')
          	}
          	chargeUsers();
        </script>
</head>
