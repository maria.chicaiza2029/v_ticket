<h1 class="text-center"> <b>REGISTRO DE EQUIPOS</b> </h1>

<form class="" action="<?php echo site_url('equipos/guardarEquipos') ?>" method="post">

  <div class="row">
    <div class="col-md-4">
      <b>ID:</b><br>
      <input type="text" required name="id_equ"
      class="form-control" placeholder="Ingrese el ID del equipo" value="">
    </div>
    <div class="col-md-4">
      <b>NOMBRE DEL EQUIPO A :</b><br>
      <input type="text" required name="nombre_equ"
      class="form-control" placeholder="Ingrese el nombre del equipo" value="">
    </div>
  </div>

  <br>

  <!-- Si necesitas la ubicación del estadio (latitud y longitud) -->


  <br>
  <center>
    <div class="row">
      <div class="cold-md-4">
      </div>
      <div class="cold-md-7">
        <button type="submit" name="button" class="btn btn-primary">
          <i class="glyphicon glyphicon-ok"></i>
          Guardar
        </button>
        <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger">
          <i class="glyphicon glyphicon-remove"></i>
          Cancelar
        </a>
      </div>
    </div>
  </center>

</form>
