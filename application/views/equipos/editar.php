<h1 class="text-center" style="color:orange"><i class="mdi mdi-reload"></i><b> Actualizar Equipo </b></h1>

<form class="" id="frm_editar_equipo" action="<?php echo site_url('equipos/procesarActualizacion'); ?>" method="post">
    <div class="row">
      <input type="hidden" name="id_es" id="id_es" value="<?php echo $EstadioEditar->id_es; ?>">
      <div class="col-md-6">
          <label for="">ID:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el id del equipo"
          class="form-control"
          required
          name="id_equ" value="<?php echo $EstadioEditar->id_equ; ?>"
          id="id_equ">
      </div>
      <div class="col-md-6">
          <label for="">NOMBRE DEL EQUIPO:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del equipo"
          class="form-control"
          required
          name="nombre_equ" value="<?php echo $EquipoEditar->nombre_equ; ?>"
          id="nombre_equ">nombre_equ
      </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-warning">
            <i class="mdi mdi-reload"></i>
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/equipos/index"
              class="btn btn-danger">
              <i class="mdi mdi-close-octagon"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_editar_equipo").validate({
   rules:{
     id_equ:{
       required:true,
       minlength:3,
       maxlength:50
     },
     nombre_equ:{
       required:true,
       minlength:3,
       maxlength:255
     }
   }
 });
</script>
