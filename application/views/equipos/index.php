<div class="row">
  <div class="col-mod-8">
    <h1 class="text-center">EQUIPOS</h1>
  </div>
  <center>
  <div class="col-md-4">
    <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar nuevo Equipo
    </a>
  </div>
</center>
</div>
<br>
<?php if ($listadoEquipo): ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
           <tr style="background-color: #2FD2AF">
             <th class="text-center">ID</th>
             <th class="text-center">NOMBRE EQUIPO</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoEquipo->result() as $equipoTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $equipoTemporal->id_equ; ?>
              </td>
              <td class="text-center">
                <?php echo $equipoTemporal->nombre_equ; ?>
              </td>

              <td class="text-center">
                <a href="<?php echo site_url('equipos/editar');?>/<?php echo $equipoTemporal->id_equ; ?>" class="btn btn-warning">
                  <i class="glyphicon glyphicon-edit"></i> Editar
                </a>
                <a href="<?php echo site_url('equipos/borrar');?>/<?php echo $equipoTemporal->id_equ; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro que desea eliminar?');">
                  <i class="glyphicon glyphicon-trash"></i> Eliminar
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay Equipos registrados</h1>
<?php endif; ?>
