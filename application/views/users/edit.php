<div class="slider-area slider-bg ">
    <div class="single-slider d-flex align-items-center slider-height2 ">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center pt-50">
                        <h2><b>Update Users</b></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Shape -->
    <div class="slider-shape d-none d-lg-block">
        <img class="slider-shape1" src="<?php echo base_url(); ?>/assets/assets/img/hero/top-left-shape.png" alt="">
        <img class="slider-shape2" src="<?php echo base_url(); ?>/assets/assets/img/hero/right-top-shape.png" alt="">
        <img class="slider-shape3" src="<?php echo base_url(); ?>/assets/assets/img/hero/left-botom-shape.png" alt="">
    </div>
</div>
<br><br><br>
<div class="row">
  <div class="col-md-12">
    <div class="container">
      <?php if($userEdit):?>

      <form class="" action="<?php echo site_url("users/processUpdate"); ?>" method="post" id="frm_edit_user">
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">ID</label>
          </div>
          <div class="col-md-7">
            <input type="hidden" name="id_use" id="id_use" value="<?php echo $userEdit->id_use; ?>" class="form-control" placeholder=" "required>
          </div>
        </div>
        <div class="row">

            <div class="col-md-4 text-right">
              <label for="">NAME</label>
            </div>
            <div class="col-md-7">
              <input type="text" name="name_use"id="name_use" value="<?php echo $userEdit->name_use; ?>" class="form-control" placeholder="Input the name please"required>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">LAST NAME</label>
            </div>
            <div class="col-md-7">
              <input type="text" name="last_name_use" id="last_name_use"value="<?php echo $userEdit->last_name_use; ?>" class="form-control" placeholder="Input the last name please"required>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">IDENTIFICATION</label>
            </div>
            <div class="col-md-7">
              <input type="number" name="identification_use" id="identification_use"value="<?php echo $userEdit->identification_use; ?>" class="form-control" placeholder="Input Identification please"required>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">EMAIL</label>
            </div>
            <div class="col-md-7">
              <input type="email" name="email_use" id="email_use"value="<?php echo $userEdit->email_use; ?>" class="form-control" placeholder="Input the email please"required>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">PHONE</label>
            </div>
            <div class="col-md-7">
              <input type="number" name="phone_use" id="phone_use"value="<?php echo $userEdit->phone_use; ?>" class="form-control" placeholder="Input the phone please"required>
            </div>
          </div>
          <br>
          <br>
          <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-7">
              <button type="submit" name="button" class="btn btn-warning"> <i class="glyphicon glyphicon-ok"></i>
                UPDATE </button>
              <a href="<?php echo site_url('users/index'); ?>" class="btn btn-danger">
                <i class=" glyphicon glyphicon-remove"></i>
                CANCEL
              </a>
            </div>
          </div>
      </form>
    <?php else:?>
      <p>Not found data</p>
    <?php endif;?>
    </div>
  </div>
</div>
<style media="screen">
  span.error{ color: red; }
</style>

<script type="text/javascript">
  $("#frm_new_location").validate({
    rules:{
      name_use:{
        required:true
      },
      last_name_use:{
        required:true
      },
      identification_use:{
        required:true
      },
      email_use:{
        required:true
      },
      phone_use:{
        required:true
      }
    },
    messages:{
      name_use:{
        required:"<br>Input the name please"
      },
      last_name_use:{
        required:"<br>Input the last name please"
      },
      identification_use:{
        required:"<br>Input the identification please"
      },
      email_use:{
        required:"<br>Input the email please"
      },
      phone_use:{
        required:"<br>Input the phone please"
      }
    },
    errorElement : 'span'
  });
</script>
