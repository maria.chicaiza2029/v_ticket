
<section class="breadcrumb-section set-bg" data-setbg="<?php echo base_url(); ?>assets/img/breadcrumb-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bs-text">
                        <h2>Tickets</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="">
           <!-- left Contents -->
           <!-- <div class="starups"> -->
             <div class="row">
               <div class="col-md-12 text-center">

                 <legend><h3><b>List of Tickets</b></h3></legend>
               </div>
             </div>

            <div class="row">

             <div class="col-md-12 text-right">
               <center>
               <div class="slider-btns">
                    <!-- Hero-btn -->
                    <a data-animation="fadeInLeft" data-delay="1s"class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Tickets</a>
               </div>
               <br>
               <!-- <div class="slider-btns">
                    <!-- Hero-btn -->
                    <!-- <a data-animation="fadeInLeft" data-delay="1s"  class="btn site-btn" onclick="chargelocations();">Update Location</a> -->
               </div> -->
               <!-- <button type="button" class="site-btn" data-toggle="modal" data-target="#myModal">AGREGAR SUCURSAL</button>
               <button type="button" name="button" class="site-btn" onclick="cargarSucursales();"> -->

               <!-- </button> -->
                  </center>
             </div>

            </div>
          <div class="row">


           <div class="col-md-12 text-right">
             <br>
             <div class="" id="container_list_tickets">

             </div>
           </div>
          </div>
        </div>
           <!--Right Contents  -->
       </section>
       <!--All startups End -->
       <!-- work company Start-->
       <!-- Modal -->
       <div id="myModal" class="modal fade" role="dialog" color text="black">
         <div class="modal-dialog modal-lg">

           <!-- Modal content-->
           <div class="modal-content">
             <div class="modal-header">
               <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
               <h4 class="modal-title"><b>New Ticket</b></h4>
             </div>
             <div class="modal-body">
               <center><h4>Form of the new Ticket</h4></center>
       				<!--<center><h3><FONT COLOR="white">NUEVO CLIENTE</FONT></h3><br></center>-->
              <!-- <center><h3><FONT COLOR="black">NUEVO CLIENTE</FONT></h3><br></center> -->
       				<form class="" enctype="multipart/form-data" action="<?php echo site_url(); ?>/tickets/saveTicket" method="post" id="frm_new_location">
       		      <center><table class="" style="width:700px;">
                  <tr>

                    <td>
                <label for="fk_id_use"><h6>USER:</h6></label>
            </td>
            <td>
                <select name="fk_id_use" id="fk_id_use" class="form-control" required>
                    <option value="">*Select a user*</option>
                    <?php foreach ($users as $usuario): ?>
                        <option value="<?php echo $usuario->id_use; ?>"><?php echo $usuario->name_use; ?> <?php echo $usuario->last_name_use; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>

            <tr>
                <td>
                    <label for="fk_id_mat"><h6>MATCHES:</h6></label>
                </td>
                <td>
                    <select name="fk_id_mat" id="fk_id_mat" class="form-control" required>
                        <option value="">*Select a match*</option>
                        <?php foreach ($matches as $partido): ?>
                            <option value="<?php echo $partido->id_mat; ?>"><?php echo $partido->comment_mat; ?>[<?php echo $partido->id_mat; ?>]</option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <label for="fk_id_loc"><h6>LOCATION:</h6></label>
                </td>
                <td>
                    <select name="fk_id_loc" id="fk_id_loc" class="form-control" required>
                        <option value="">*Select a location*</option>
                        <?php foreach ($locations as $ubicacion): ?>
                            <option value="<?php echo $ubicacion->id_loc; ?>"><?php echo $ubicacion->name_loc; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>



       				</table></center>
             </div>
             <div class="modal-footer">
       				<div class="row">
       					<div class="col-md-12">
       						<button type="submit" name="button" class="btn btn-success">Save Ticket</button>
       						<button type="button" onclick="cerrarModal();"
       										name="button" class="btn btn-danger">Cancel</button>
       					</div>
       					</form>
       					<!--<div class="col-md-4">
       	        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       					</div>-->
             </div>
           </div>
       	</div>
         </div>
       </div>
       <script type="text/javascript">
       $("#myModal").appendTo("body");
       //Funcion para ocultar/cerrar la ventana Modal
       	function cerrarModal(){
       		$("#myModal").modal("hide");
       	}
       </script>
       <style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_new_ticket").validate({
    rules:{
      name_loc:{
        required:true
      },
      cost_loc:{
        required:true
      }
    },
    messages:{
      name_loc:{
        required:"<br>Input the name of tickets please"
      },
      cost_loc:{
        required:"<br>Input the cost of ticket please"
      }
    },
    submitHandler:function(form){
          var url=$(form).prop("action");//capturando url (controlador/funcion)
          //generando peticion asincrona
          $.ajax({
               url:url,//action del formulario
               type:'post',//definiendo el tipo de envio de datos post/get
               data:$(form).serialize(), //enviando los datos ingresados en el formulario
               success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                  //alert('Cliente guardado exitosamente');
									chargetickets(); //llamado a la funcion para actualizar listado de clientes
									$(form)[0].reset();
									cerrarModal();
									Swal.fire({
										background: '#0C062E',
										color:'#FFF',
										title: 'Confirmación',
									  text: "Ticket save sucessfully",
										icon:'success',
									});
               },
               error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                  //alert('Error al insertar, intente nuevamente');
               }
          });
        },
		errorElement : 'span'
  });
</script>
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Are you Sure?',
  text: "¡It will be forever!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Delete!',
  cancelButtonText:'Cancel',
	background: '#0C062E',
	color:'#FFF' }).then((result) => {
  if (result.isConfirmed) {
		chargetickets();
    window.ticket.href = urlToRedirect;
  }
});
}
</script>
<script type="text/javascript">
          	function chargetickets(){
          		$("#container_list_tickets").load('<?php echo site_url("tickets/tableTickets2") ?>')
          	}
          	chargetickets();
        </script>
</head>
