<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("user");
	}

  public function index(){
  // $data ["listadoClientes"]=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('users/index');
    $this->load->view("footer");
  }
  public function tableUsers()
  {
    $data ["listUsers"]=$this->user->getAll();
    $this->load->view('users/tableUsers',$data);//pasando parametros a la vista
  }
public function  saveUser(){
  $dataUser=array(
    "name_use"=>$this->input->post('name_use'),
    "last_name_use"=>$this->input->post('last_name_use'),
    "identification_use"=>$this->input->post('identification_use'),
    "email_use"=>$this->input->post('email_use'),
    "phone_use"=>$this->input->post('phone_use'),
  );
  print_r($dataUser);
		if($this->user->input($dataUser)){

	  $this->session->set_flashdata('confirmation','User was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('users/index');
}
public function delete($id_loc){
    if($this->user->deleteForId($id_loc)){
		$this->session->set_flashdata('confirmation','User was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('users/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data["userEdit"]=$this->user->getForId($id);
		$this->load->view("header");
		$this->load->view("users/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_use=$this->input->post('id_use');
		$dataUserUpdate=array(
      "name_use"=>$this->input->post('name_use'),
      "last_name_use"=>$this->input->post('last_name_use'),
      "identification_use"=>$this->input->post('identification_use'),
      "email_use"=>$this->input->post('email_use'),
      "phone_use"=>$this->input->post('phone_use'),
		);
		if($this->user->edit($id_use,$dataUserUpdate)){
			$this->session->set_flashdata('confirmation','User was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('users/index');
	}

}//cierre de la clase
