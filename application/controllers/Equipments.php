<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipments extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("equipment");
	}

  public function index(){
  // $data ["listadoClientes"]=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('equipments/index');
    $this->load->view("footer");
  }
  public function tableEquipments()
  {
    $data ["listEquipments"]=$this->equipment->getAll();
    $this->load->view('equipments/tableEquipments',$data);//pasando parametros a la vista
  }
public function  saveEquipment(){
  $dataEquipment=array(
    "name_equ"=>$this->input->post('name_equ'),
  );
  print_r($dataEquipment);
		if($this->equipment->input($dataEquipment)){

	  $this->session->set_flashdata('confirmation','Equipment was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('equipments/index');
}
public function delete($id_equ){
    if($this->equipment->deleteForId($id_equ)){
		$this->session->set_flashdata('confirmation','Equipment was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('equipments/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data["equipmentEdit"]=$this->equipment->getForId($id);
		$this->load->view("header");
		$this->load->view("equipments/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_equ=$this->input->post('id_equ');
		$dataEquipmentUpdate=array(
      "name_equ"=>$this->input->post('name_equ'),
		);
		if($this->equipment->edit($id_equ,$dataEquipmentUpdate)){
			$this->session->set_flashdata('confirmation','Equipment was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('equipments/index');
	}

}//cierre de la clase
