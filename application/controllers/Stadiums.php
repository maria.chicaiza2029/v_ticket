<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stadiums extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("stadium");
	}

  public function index(){
  // $data ["listadoClientes"]=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('stadiums/index');
    $this->load->view("footer");
  }
  public function tableStadiums()
  {
    $data ["listStadiums"]=$this->stadium->getAll();
    $this->load->view('stadiums/tableStadiums',$data);//pasando parametros a la vista
  }
public function  saveStadium(){
  $dataStadium=array(
    "name_sta"=>$this->input->post('name_sta'),
    "address_sta"=>$this->input->post('address_sta'),
    "abiliti_sta"=>$this->input->post('abiliti_sta'),
  );
  print_r($dataStadium);
		if($this->stadium->input($dataStadium)){

	  $this->session->set_flashdata('confirmation','stadium was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('stadiums/index');
}
public function delete($id_sta){
    if($this->stadium->deleteForId($id_sta)){
		$this->session->set_flashdata('confirmation','stadium was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('stadiums/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data["stadiumEdit"]=$this->stadium->getForId($id);
		$this->load->view("header");
		$this->load->view("stadiums/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_sta=$this->input->post('id_sta');
		$dataStadiumUpdate=array(
      "name_sta"=>$this->input->post('name_sta'),
      "address_sta"=>$this->input->post('address_sta'),
      "abiliti_sta"=>$this->input->post('abiliti_sta'),

		);
		if($this->stadium->edit($id_sta,$dataStadiumUpdate)){
			$this->session->set_flashdata('confirmation','Estadium was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('stadiums/index');
	}

}//cierre de la clase
