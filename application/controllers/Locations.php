<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("location");
	}

  public function index(){
  // $data ["listadoClientes"]=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('locations/index');
    $this->load->view("footer");
  }
  public function tableLocations()
  {
    $data ["listLocations"]=$this->location->getAll();
    $this->load->view('locations/tableLocations',$data);//pasando parametros a la vista
  }
public function  saveLocation(){
  $dataLocation=array(
    "name_loc"=>$this->input->post('name_loc'),
    "cost_loc"=>$this->input->post('cost_loc'),
  );
  print_r($dataLocation);
		if($this->location->input($dataLocation)){

	  $this->session->set_flashdata('confirmation','Location was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('locations/index');
}
public function delete($id_loc){
    if($this->location->deleteForId($id_loc)){
		$this->session->set_flashdata('confirmation','Location was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('locations/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data["locationEdit"]=$this->location->getForId($id);
		$this->load->view("header");
		$this->load->view("locations/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_loc=$this->input->post('id_loc');
		$dataLocationUpdate=array(
      "name_loc"=>$this->input->post('name_loc'),
      "cost_loc"=>$this->input->post('cost_loc'),
		);
		if($this->location->edit($id_loc,$dataLocationUpdate)){
			$this->session->set_flashdata('confirmation','Location was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('locations/index');
	}

}//cierre de la clase
