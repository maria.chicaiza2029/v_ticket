<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("ticket");
		$this->load->model("matche");
		$this->load->model("user");
			$this->load->model("location");

	}

	public function index() {
	    $data['matches'] = $this->matche->getAll2()->result();
	    $data['users'] = $this->user->getAll()->result();
	    $data['locations'] = $this->location->getAll()->result();
 			$data['tickets'] = $this->ticket->getAll()->result();
	    $this->load->view('header');
			  //$this->load->view('tickets/tableTickets',$data);//pasando parametros a la vista
	    $this->load->view('tickets/index', $data);
	    $this->load->view("footer");
	}

  public function tableTickets2()
  {
    $data ["listTickets"]=$this->ticket->getAll();
    $this->load->view('tickets/tableTickets',$data);//pasando parametros a la vista
  }
public function  saveTicket(){
  $dataTicket=array(
    "fk_id_use"=>$this->input->post('fk_id_use'),
    "fk_id_mat"=>$this->input->post('fk_id_mat'),
    "fk_id_loc"=>$this->input->post('fk_id_loc'),
  );
  print_r($dataTicket);
		if($this->ticket->input($dataTicket)){

	  $this->session->set_flashdata('confirmation','Ticket was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('tickets/index');
}
public function delete($id_tic){
    if($this->ticket->deleteForId($id_tic)){
		$this->session->set_flashdata('confirmation','Ticket was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('tickets/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data['matches'] = $this->matche->getAll2()->result();
		$data['users'] = $this->user->getAll()->result();
		$data['locations'] = $this->location->getAll()->result();
		//$data['tickets'] = $this->ticket->getAll()->result();
		$data["ticketEdit"]=$this->ticket->getForId($id);
		$this->load->view("header");
		$this->load->view("tickets/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_tic=$this->input->post('id_tic');
		$dataTicketUpdate=array(
      "fk_id_use"=>$this->input->post('fk_id_use'),
      "fk_id_mat"=>$this->input->post('fk_id_mat'),
      "fk_id_loc"=>$this->input->post('fk_id_loc'),
		);
		if($this->ticket->edit($id_tic,$dataTicketUpdate)){
			$this->session->set_flashdata('confirmation','Ticket was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('tickets/index');
	}

}//cierre de la clase
