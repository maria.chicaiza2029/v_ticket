<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partidos extends CI_Controller {

  public function __construct(){
  parent::__construct();
  $this->load->model("equipo");
}

	public function index()
	{
    $data["listadoEquipo"]=
    $this->equipo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('equipos/index',$data);
		$this->load->view('footer');
	}

  public function nuevo()
  {

    $this->load->view('header');
    $this->load->view('equipos/nuevo');
    $this->load->view('footer');
  }

  public function guardarEquipo(){
		$datosNuevosEquipo=array(
      "id_equ"=>$this->input->post('id_equ'),
      "nombre_equ"=>$this->input->post('nombre_equ'),


		);
		print_r($datosNuevosEquipo);
		if($this->equipo->insertar($datosNuevosEquipo)){
			redirect('equipos/index');
		}else {
			echo "<h1>Error</h1>";
		}
}
public function borrar($id_equ){
	if ($this->equipo->eliminarPorId($id_equ)) {
		echo "Eliminado exitosamente";
		redirect('equipos/index');
	} else {
		echo "Error al Eliminar :(";
	}
}

//fucion para renderizar firmularion de actualizacion
public function editar($id){
$data["EquipoEditar"]=$this->equipo->obtenerPorId($id);
$this->load->view('header');
$this->load->view('equipos/editar',$data);
$this->load->view('footer');
}



public function procesarActualizacion(){
		$datosEquipoEditado=array(
      "id_equ"=>$this->input->post('id_equ'),
      "nombre_equ"=>$this->input->post('nombre_equ'),
		);
	 $id=$this->input->post("id_equ");
		if($this->equipo->editar($id,$datosEquipoEditado)){
			redirect('equipos/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}
}//cerrar
