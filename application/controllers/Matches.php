<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matches extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("matche");
		$this->load->model("equipment");
		$this->load->model("stadium");
	}

	public function index(){
		$data['matches'] = $this->matche->getAll();
	    $data['equipos'] = $this->equipment->getAll();
	    $data['estadios'] = $this->stadium->getAll();
	    $this->load->view('header');
	    $this->load->view('matches/index', $data);
	    $this->load->view('footer');
	}


  public function tableMatches()
  {
    $data ["listMatches"]=$this->matche->getAll();
    $this->load->view('matches/tableMatches',$data);//pasando parametros a la vista
  }

public function  saveMatche(){
  $dataMatche=array(
    "fk_id_equ"=>$this->input->post('fk_id_equ'),
    "fk_id_sta"=>$this->input->post('fk_id_sta'),
    "date_mat"=>$this->input->post('date_mat'),
		"comment_mat"=>$this->input->post('comment_mat')
  );
  print_r($dataMatche);
		if($this->matche->input($dataMatche)){

	  $this->session->set_flashdata('confirmation','Location was entered sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to input, please try again');

		}
		redirect('matches/index');
}
public function delete($id_mat){
    if($this->matche->deleteForId($id_mat)){
		$this->session->set_flashdata('confirmation','Location was entered sucessfully');//primer parametro nombre de la variable
    }else{
		$this->session->set_flashdata('error','error to input, please try again');
    }
	redirect('matches/index');
  }
	//renderisar formulario de actualizacion
	public function edit($id){
		$data['equipos'] = $this->equipment->getAll();
		$data['estadios'] = $this->stadium->getAll();
		$data["matcheEdit"]=$this->matche->getForId($id);
		$this->load->view("header");
		$this->load->view("matches/edit",$data);
		$this->load->view("footer");
		// code...
	}
	public function processUpdate(){
		$id_mat=$this->input->post('id_mat');
		$dataMatcheUpdate=array(
      "fk_id_equ"=>$this->input->post('fk_id_equ'),
      "fk_id_sta"=>$this->input->post('fk_id_sta'),
      "date_mat"=>$this->input->post('date_mat'),
				"comment_mat"=>$this->input->post('comment_mat')
		);
		if($this->matche->edit($id_mat,$dataMatcheUpdate)){
			$this->session->set_flashdata('confirmation','Matches was updated sucessfully');//primer parametro nombre de la variable
		}else{
			$this->session->set_flashdata('error','error to update, please try again');

		}
		redirect('matches/index');
	}


}//cierre de la clase
